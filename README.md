# AOP语法检测工具（PointCut Expression Check Tool）
小工具：检测切入点（pointcut expression）是否能匹配到指定类的指定方法

![使用截图](resources/pointcut2.png)

## 使用方法：
打开小工具，有三个输入框。分别输入对应的值。

Expression：输入切入点语法（pointcut expression）。例如：execution(* com.kvn.core.car..*.*(..))

Class FullPath：输入类的全限定名。例如：com.kvn.core.car.controller.CarController

Method：输入需要匹配的方法名称。如果不输入，则默认使用defaultMethod


最后点击check按钮，就可以看匹配结果了。





## 实现技术：
javaFX + ASM

## 下载地址：
<https://gitee.com/kkk001/pointcutExpCheck/blob/master/resources/pointcutExpCheck_v1.0.rar>